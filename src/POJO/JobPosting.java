/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

/**
 *
 * @author Jack
 */
public class JobPosting {

    private String title;
    private String company;
    private String location;
    private String summary;
    private String url;
    private boolean isMissingInfo;

    public JobPosting(String title, String company, String location, String summary, String url) {
        this.title = title;
        this.company = company;
        this.location = location;
        this.summary = summary;
        this.url = url;
        
       
        analyzeInfo();
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     * @param summary the summary to set
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the isMissingInfo
     */
    public boolean isIsMissingInfo() {
        return isMissingInfo;
    }

    /**
     * @param isMissingInfo the isMissingInfo to set
     */
    public void setIsMissingInfo(boolean isMissingInfo) {
        this.isMissingInfo = isMissingInfo;
    }

    /*Only checks for these 4 because there will always be a link*/
    private void analyzeInfo() {
        int count = 4;
        if (title.isEmpty()) {
            count--;
        }
        if (company.isEmpty()) {
            count--;
        }
        if (location.isEmpty()) {
            count--;
        }
        if (summary.isEmpty()) {
            count--;
        }
        if (count < 3) {
            isMissingInfo = true;
        }else{
            isMissingInfo= false;
        }
    }
}
