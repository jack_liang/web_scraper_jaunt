package POJO;

import com.jaunt.Element;
import com.jaunt.Elements;
import com.jaunt.HttpResponse;
import com.jaunt.JauntException;
import com.jaunt.NotFound;
import com.jaunt.ResponseException;
import com.jaunt.UserAgent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import static webcrawler.Driver.timeStamp;

/**
 *
 * @author Jack
 */
public class WebCrawler {

    Map title_keywords;
    Map company_keywords;
    Map location_keywords;
    Map summary_keywords;

    public WebCrawler() {
        company_keywords = load_keywords("company.txt");
        location_keywords = load_keywords("location.txt");
        summary_keywords = load_keywords("summary.txt");
        title_keywords = load_keywords("title.txt");
    }

    private Map load_keywords(String fileName) {
        Map keywords = Collections.synchronizedMap(new HashMap());

        try {

            String cursor;
            InputStream in = getClass().getResourceAsStream("/Resources/" + fileName);
            Reader fr = new InputStreamReader(in, "utf-8");
            BufferedReader br = new BufferedReader(fr);

            while ((cursor = br.readLine()) != null) {
                //  System.out.println(cursor);
                keywords.put(cursor, "");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return keywords;
    }

    /*Example 1*/
    /*consider a printhtml where a "contains href " is used*/
    public void printHTML(String url) {
        try {
            UserAgent userAgent = new UserAgent();                       //create new userAgent (headless browser).
            userAgent.visit(url);                        //visit a url  
            System.out.println(userAgent.doc.innerHTML());               //print the document as HTML
        } catch (JauntException e) {         //if an HTTP/connection error occurs, handle JauntException.
            System.err.println(e);
        }
    }

    public Set<String> findEveryJobHref(String url) {
        Set<String> job_links = new LinkedHashSet<>();
        try {
            UserAgent userAgent = new UserAgent();
            userAgent.visit(url);
            Elements elements = userAgent.doc.findEvery("<a href>");             //find all hrefs in the document

            for (Element e : elements) {
                String link = e.getAt("href");
                /*
                 Since the page can have multiple different links,this if statement is used to filter out what is a job page and what isnt.
                 Currently the conditions are set to only take links that are job pages that belong to indeed.com
                 */
                if (isJobPage(link)) {
                    job_links.add(link);
                }
            }
            /*elements now contains every href in the document*/
        } catch (JauntException e) {
            System.err.println(e);
        }
        return job_links;
    }

    /*Currently using only title tag to track down the job title*/
    public String getTitle(String url) {
        try {
            UserAgent userAgent = new UserAgent();                       //create new userAgent (headless browser).
            userAgent.visit(url);                        //visit a url.
            String title = userAgent.doc.findFirst("<title>").getText(); //get child text of title element.
            //System.out.println("\nTitle: " + title);    //print the title
            return title;

        } catch (JauntException e) {   //if title element isn't found or HTTP/connection error occurs, handle JauntException.
            System.err.println(e);
        }
        return "";
    }

    /*
     The following 4 get methods are for extracting job title,location, company and job summary/description given the first element of the DOM.
     Simply use findFirst("<html>") to get the first element
     */
    public String getTitle(Element e) {
        return recursiveNodeTraversal(e, title_keywords);
    }

    public String getLocation(Element e) {
        return recursiveNodeTraversal(e, location_keywords);
    }

    public String getCompany(Element e) {
        return recursiveNodeTraversal(e, company_keywords);
    }

    public String getSummary(Element e) {
        /*Give back the URL if no summary found*/
        return recursiveNodeTraversal(e, summary_keywords);
    }
    /*Theoretically concept is sound, needs testing, next step is solidifying the getTitle function with POS tagger*/

    public String recursiveNodeTraversal(Element e, Map keywords) {
        /*if its null node, we dont want to do anything to it,just return*/
        String target = "";
        if (e == null) {
            System.out.println("There is no match");
            return target;
        }

        try {
            if (e.hasAttribute("id")) {
                String a = e.getAt("id");//value of the attribute
                if (keywords.containsKey(a)) {
                    System.out.println("Match id " + a + "\n" + e.innerText().trim());
                    return e.innerText().trim();
                }
            }
            if (e.hasAttribute("itemprop")) {
                String a = e.getAt("itemprop");//value of the attribute
                if (keywords.containsKey(a)) {
                    System.out.println("Match itemprop " + a + "\n" + e.innerText().trim());

                    return e.innerText().trim();
                }
            }
            if (e.hasAttribute("class")) {
                String[] styles = e.getAt("class").split(" ");
                for (String style : styles) {
                    if (keywords.containsKey(style)) {
                        System.out.println("Match id " + style + "\n" + e.innerText().trim());

                        return e.innerText().trim();

                    }
                }

            }
            if (e.hasAttribute("name")) {
                String a = e.getAt("name");//value of the attribute
                if (keywords.containsKey(a)) {
                    System.out.println("Match name " + a + "\n" + e.innerText().trim());

                    return e.innerText().trim();
                }
            }

            /*
             This means its not null
             check the attributes, if none of them are a hit
             continue on with the kids
             */
            List<Element> e_kids = e.getChildElements();
            for (Element e_kid : e_kids) {
                target = recursiveNodeTraversal(e_kid, keywords);
                if (target.length() != 0) {
                    return target;
                }
            }

        } catch (NotFound ex) {
            Logger.getLogger(WebCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return target;

    }

    public JobPosting extractJobPosting(String url) {
        try {
            UserAgent userAgent = new UserAgent();
            userAgent.visit(url);
            Element root = userAgent.doc.findFirst("<html>"); //get child text of title element.

            /*Getting title,location,company and summary*/
            System.out.println("\n\ngetting title");
            String title = getTitle(root);
            
            System.out.println("\n\n getting location");
            String location = getLocation(root);
            
            System.out.println("\n\n getting company");
            String company = getCompany(root);
            
            System.out.println("\n\n Getting summary");
            String summary = getSummary(root);

            JobPosting jp = new JobPosting(title,  company,location, summary, url);
            return jp;

        } catch (JauntException e) {

        }
        return null;

    }

    /**
     * Takes Element e and returns a map containing attributes and values
     *
     * @param e
     * @return
     */
    public Map<String, String> hashAttributes(Element e) {
        Map m = Collections.synchronizedMap(new HashMap());
        List<String> a = e.getAttributeNames();
        a.stream().forEach((attribute) -> {
            try {
                String val = e.getAt(attribute);
                m.put(attribute, val);
            } catch (NotFound ex) {
                Logger.getLogger(WebCrawler.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        return m;
    }

    public void seeXML(String url) {
        try {
            UserAgent userAgent = new UserAgent();                       //create new userAgent (headless browser).
            userAgent.visit(url);                        //visit a url  
            System.out.println(userAgent.doc.innerXML());               //print the document as HTML
        } catch (JauntException e) {         //if an HTTP/connection error occurs, handle JauntException.
            System.err.println(e);
        }
    }

    /*Starts at root of element 
     recursively looks for element with keyword "pagination" 
     returning null means it doesnt exist*/
    public Element getPaginationElement(Element e) {
        try {
            if (e.hasAttribute("id")) {
                String a = e.getAt("id");//value of the attribute
                if (a.contains("pagination")) {
                    return e;
                }
            }
            if (e.hasAttribute("itemprop")) {
                String a = e.getAt("itemprop");//value of the attribute
                if (a.contains("pagination")) {
                    return e;
                }
            }
            if (e.hasAttribute("class")) {
                String[] styles = e.getAt("class").split(" ");
                for (String style : styles) {
                    if (style.contains("pagination")) {
                        return e;
                    }
                }
            }
            if (e.hasAttribute("name")) {
                String a = e.getAt("name");//value of the attribute
                if (a.contains("pagination")) {

                    return e;
                }
            }
            if (e.getChildElements() == null) {
                return null;
            }
            List<Element> e_kids = e.getChildElements();
            for (Element e_kid : e_kids) {
                Element target = getPaginationElement(e_kid);
                if (target != null) {
                    return target;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public Set<String> getPaginations(String url, Set s) {
        /*
         Visit page, look for paginated section, get all hrefs , 
         check if they exist as a key in map, if they dont, add them as false value
         value is only true when they have already been visited. 
         after joblinks on page are done, check paginated values
         */
        //System.out.println("Getting paginations of : "+url);
        /*HashSet gave me greatest to least, linkedhashset gave me least to greatest*/
        Set<String> pages = s;
        try {
            UserAgent userAgent = new UserAgent();
            userAgent.visit(url);
            if (userAgent.response != null) {
                Element root = userAgent.doc.findFirst("<html>");

                Element paginate = getPaginationElement(root);
                if (paginate == null) {
                    System.out.println("There are no paginations");
                    return s;
                } else {
                    Elements links = paginate.findEach("<a href>");
                    for (Element page : links) {
                        String link = page.getAt("href");
                        if (!pages.contains(link)) {
                            pages.add(link);
                            getPaginations(link, pages);
                        }
                    }
                    /*Now for this one, we will run through each pagination to add the links that are missing*/
                    //pages.stream().forEach((link) -> { System.out.println(link);});//display pages

                }
            }
        } catch (ResponseException ex) {
            HttpResponse response = ex.getResponse();               //or check userAgent.response
            if (response != null) {                                    //print response data field by field
                System.err.println("Requested url: " + response.getRequestedUrlMsg()); //print the requested url
                System.err.println("HTTP error code: " + response.getStatus());        //print HTTP error code
                System.err.println("Error message: " + response.getMessage());         //print HTTP status message
            } else {
                System.out.println("Connection error, no response!");
            }
        } catch (NotFound ex) {
            Logger.getLogger(WebCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pages;
    }
    /*Returns true if the page is not a review*/

    public boolean isJobPage(String url) {
        //indeed
        return isIndeedJobPage(url) || isMonsterPage(url) || ((url.contains("com/pagead")) && url.contains("indeed"));
    }

    public boolean isMonsterPage(String url) {
        return url.contains("monster") && url.contains("jobPosition");
    }

    /*returns true if the page is strictly hosted by indeed.com and not external page*/
    public boolean isIndeedJobPage(String url) {
        return (url.contains("indeed"))
                && (url.contains("com/cmp"))
                && (!url.contains("/review"))
                && (!url.contains("com/pagead"))
                && (!url.contains("com/rc"))
                && (!url.contains("campaignid="));
    }

    public void scrape(String url) {
        System.out.println(timeStamp() + " getting result pages...");
        timeStamp();
        Set<String> result_pages = getPaginations(url, new LinkedHashSet<>());
        System.out.println(timeStamp() + " result pages retrieved.");

        /*This is for storing individual links of job pages contained in the result pages*/
        System.out.println(timeStamp() + " Extracting job links from the result pages...");

        Set<String> job_links = new LinkedHashSet<>();
        result_pages.stream().forEach((result_page) -> {
            job_links.addAll(findEveryJobHref(result_page));
        });
        System.out.println(timeStamp() + " Job links retrieved..." + job_links.size() + " Jobs available");

        //job_links.stream().forEach((job) -> {System.out.println(job);});
        System.out.println(timeStamp() + " Initiating formatting into json...");

        HashMap<String, JobPosting> jobs = new HashMap<>();
        String json = "";
        System.out.println();
        int count = 0;
        Set<String> faulty_links = new HashSet<>();
        /*This needs to be configured so it prints the data in json format*/
        for (String link : job_links) {
            try {
                if (isValid(link)) {
                    JobPosting jp = extractJobPosting(link);
                    /*Only want the job posting if the webpage has all 4 data fields instead of partial*/
                    if (!jp.isIsMissingInfo()) {
                        jobs.put(link, jp);
                        count++;
                        json += "\n\nTitle:" + jp.getTitle()
                                + "\n       Company:" + jp.getCompany()
                                + "\n       Location:" + jp.getLocation()
                                + "\n       Link:" + jp.getUrl()
                                + "\n       Summary:" + jp.getSummary();
                    }
                } else {
                    faulty_links.add(link);
                }
            } catch (Exception e) {
                System.out.println("Error at :" + link);
                faulty_links.add(link);

            }
        }
        System.out.println(timeStamp() + " Finished formatting into json.");

        System.out.println(json);
        System.out.println("There are " + count + " postings.");
        System.out.println("The links below had issues with the code:");
        faulty_links.stream().forEach((link) -> {
            System.out.println(link);
        });
    }

    public boolean isValid(String url) {
        UserAgent userAgent = new UserAgent();
        try {
            userAgent.visit(url);
            String html = userAgent.doc.innerHTML();
            return true;
        } catch (ResponseException ex) {
            /* ex.printStackTrace();
             HttpResponse response = ex.getResponse();               //or check userAgent.response
             if (response != null) {                                    //print response data field by field
             System.err.println("Requested url: " + response.getRequestedUrlMsg()); //print the requested url
             System.err.println("HTTP error code: " + response.getStatus());        //print HTTP error code
             System.err.println("Error message: " + response.getMessage());         //print HTTP status message
             } else {
             System.out.println("Connection error, no response!");
             }*/
            return false;
        }

    }
}
