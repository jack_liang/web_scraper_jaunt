/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

import com.jaunt.Element;
import com.jaunt.Elements;
import com.jaunt.JauntException;
import com.jaunt.ResponseException;
import com.jaunt.UserAgent;

/**
 *
 * @author Jack
 */

/*This project uses Jaunt*/
public class WebCrawlerPrototype {

    public WebCrawlerPrototype() {
    }

    /*Example 1*/
    public void printHTML() {
        try {
            UserAgent userAgent = new UserAgent();                       //create new userAgent (headless browser).
            userAgent.visit("http://oracle.com");                        //visit a url  
            System.out.println(userAgent.doc.innerHTML());               //print the document as HTML
        } catch (JauntException e) {         //if an HTTP/connection error occurs, handle JauntException.
            System.err.println(e);
        }
    }

    /*Example 2*/
    public void findFirst() {
        try {
            UserAgent userAgent = new UserAgent();                       //create new userAgent (headless browser).
            System.out.println("SETTINGS:\n" + userAgent.settings);      //print the userAgent's default settings.
            userAgent.settings.autoSaveAsHTML = true;                    //change settings to autosave last visited page.

            userAgent.visit("http://oracle.com");                        //visit a url.
            String title = userAgent.doc.findFirst("<title>").getText(); //get child text of title element.
            System.out.println("\nOracle's website title: " + title);    //print the title

            userAgent.visit("http://amazon.com");                        //visit another url.
            title = userAgent.doc.findFirst("<title>").getText();        //get child text of title element.
            System.out.println("\nAmazon's website title: " + title);    //print the title 
        } catch (JauntException e) {   //if title element isn't found or HTTP/connection error occurs, handle JauntException.
            System.err.println(e);
        }
    }

    /*Example 3*/
    public void getElementText() {
        try {
            UserAgent userAgent = new UserAgent();
            //open HTML from a String.
            /*String S is basically string representation of the html*/
            String s="<html><body>WebPage <div>Hobbies:<p>beer<p>skiing</div> Copyright 2013</body></html>";
            userAgent.openContent(s);
            System.out.println(s);
            Element body = userAgent.doc.findFirst("<body>");//finds start of the body tag
            Element div = body.findFirst("<div>");//finds the start of the div tag

            
            /*join child text of body tag,
            basically everything in scope of <body> but not in the <div> meaning doesnt look at nested*/
            System.out.println("body's text: " + body.getText());         
            
            /*join all text within body tag, 
            basically everything enclosed in body, nested included*/
            System.out.println("body's innerText: " + body.innerText());  
            
            /*join child text of div element
            basically those not nested*/
            System.out.println("div's text: " + div.getText());           
            
            /*join all text within the div element
            basically everything included nested data*/
            System.out.println("div's innerText: " + div.innerText());    
        } catch (JauntException e) {
            System.err.println(e);
        }
    }

    /*Example 4*/
    public void getSpecificElement() {
        try {
            UserAgent userAgent = new UserAgent();
            userAgent.visit("http://intel.com");

            Element anchor = userAgent.doc.findFirst("<a href>");            //find 1st anchor element with href attribute
            System.out.println("anchor element: " + anchor);                 //print the anchor element
            System.out.println("anchor's tagname: " + anchor.getName());     //print the anchor's tagname
            System.out.println("anchor's href attribute: " + anchor.getAt("href"));  //print the anchor's href attribute
            System.out.println("anchor's parent Element: " + anchor.getParent());    //print the anchor's parent element

            System.out.println("\n");
            Element meta = userAgent.doc.findFirst("<head>").findFirst("<meta>");    //find 1st meta element in head section
            System.out.println("meta element: " + meta);                     //print the meta element
            System.out.println("meta's tagname: " + meta.getName());         //print the meta's tagname
            System.out.println("meta's content attribute: " + meta.getAt("content"));//print the meta's content attribute
            System.out.println("meta's parent Element: " + meta.getParent());        //print the meta's parent element
        } catch (JauntException e) {
            System.err.println(e);
        }
    }

    /*using find each, wont look inside nested*/
    public void iterateResults() {
        try {
            UserAgent userAgent = new UserAgent();
            userAgent.visit("http://amazon.com");

            Elements tables = userAgent.doc.findEach("<table>");       //find non-nested tables   
            System.out.println("Found " + tables.size() + " tables:");
            for (Element table : tables) {                               //iterate through Results
                System.out.println(table.outerHTML() + "\n----\n");      //print each element and its contents
            }

            Elements ols = userAgent.doc.findEach("<table>").findEach("<ol>");//find non-nested ol's within non-nested tables
            System.out.println("Found " + ols.size() + " OLs:");
            for (Element ol : ols) {                                     //iterate through Results
                System.out.println(ol.outerHTML() + "\n----\n");         //print each element and its contents
            }
        } catch (ResponseException e) {
            System.out.println(e);
        }
    }

    /*using find every, will look inside nested*/
    public void iterateFindEvery() {
        try {
            UserAgent userAgent = new UserAgent();
            userAgent.visit("http://jaunt-api.com/examples/food.htm");

            Elements elements = userAgent.doc.findEvery("<div>");             //find all divs in the document
            System.out.println("Every div: " + elements.size() + " results"); //report number of search results.
            System.out.println(elements.innerHTML()+"\n\n\n");
            elements = userAgent.doc.findEach("<div>");                       //find all non-nested divs
            System.out.println("Each div: " + elements.size() + " results");  //report number of search results.
            //find non-nested divs within <p class='meat'>
            elements = userAgent.doc.findFirst("<p class=meat>").findEach("<div>");
            System.out.println("Meat search: " + elements.size() + " results");//report number of search results.
        } catch (JauntException e) {
            System.err.println(e);
        }
    }

    public void searchRegExp() throws ResponseException {
        UserAgent userAgent = new UserAgent();
        userAgent.visit("http://jaunt-api.com/examples/hello.htm");

        Elements elements = userAgent.doc.findEvery("<div|span>");      //find every element who's tagname is div or span.
        System.out.println("search results:\n" + elements.innerHTML()); //print the search results

        elements = userAgent.doc.findEvery("<p id=1|4>");               //find every p element who's id is 1 or 4
        System.out.println("search results:\n" + elements.innerHTML()); //print the search results

        elements = userAgent.doc.findEvery("< id=[2-6]>");              //find every element (any name) with id from 2-6
        System.out.println("search results:\n" + elements.innerHTML()); //print the search results

        elements = userAgent.doc.findEvery("<p>ho");      //find every p who's joined child text contains 'ho' (regex)
        System.out.println("search results:\n" + elements.innerHTML()); //print the search results
    }

   
}
