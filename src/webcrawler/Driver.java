/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webcrawler;

import POJO.JobPosting;
import POJO.WebCrawler;
import com.jaunt.Element;
import com.jaunt.Elements;
import com.jaunt.NotFound;
import com.jaunt.ResponseException;
import com.jaunt.UserAgent;
import java.util.Date;

/**
 *
 * @author Jack
 */
public class Driver {

    /**
     * @param args the command line arguments
     * @throws com.jaunt.ResponseException
     * @throws com.jaunt.NotFound
     */
    public static void main(String[] args) throws ResponseException, NotFound {

        WebCrawler wc = new WebCrawler();

        /*This works fine*/
        /*String starting_url = "http://www.indeed.com/jobs?q=culinary&start=990";
         Set<String>urls=wc.findEveryJobHref(starting_url); 
         urls.stream().forEach((link) -> {System.out.println(link);});*/
        System.out.println("Now we test the real thing:");
        //wc.scrape("http://www.indeed.com/jobs?q=culinary&l=");
        String url=("http://www.jobsonthemenu.com/en-us/search/?keyword=culinary&location=");
       UserAgent ua=new UserAgent();
        ua.visit(url);
        /*Elements elements=ua.doc.findEvery("<a href>");
         for (Element e : elements) {
                String link = e.getAt("href");
                if(link.contains("/job/")){
                System.out.println("////////////////////////////////////////////////////////////////////////////////////");
                System.out.println("Current Link:"+link);
                JobPosting jp=wc.extractJobPosting(link);
               // System.out.println("Title:"+jp.getTitle()+"\n       Company:"+ jp.getCompany()+"\n       Location:"+ jp.getLocation()+"\n       Summary:"+ jp.getSummary());
                }
            }*/
        
        /*Consider getting the element, 
        then recursively scan that element for possible tags with nested keywords*/
           wc.extractJobPosting("http://www.jobsonthemenu.com/en-us/job/United-States/Culinary-Associates-Program-CAP-College-Programs/J3G2X16RSYBK44459FY/");
       
       
    }

    public static String timeStamp() { 
        Date now = new Date();
        return (now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds());

    }

    public static void checkSettings() {
        UserAgent userAgent = new UserAgent();
        System.out.println("SETTINGS:\n" + userAgent.settings);
        userAgent.settings.autoSaveAsHTML = true;
    }
}
